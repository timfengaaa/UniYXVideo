//
//  GdpaVideoSdk.m
//  video
//
//  Created by Timfeng on 2021/1/29.
//


#import "GdpaVideoHeader.h"
#import "NEMediaCaptureEntity.h"

@implementation GdpaVideoSdk{
    LSMediaCapture* _mediaCapture;
}

/**
 是否有权限
 */
- (BOOL)requestMediaCapturerAccessWithCompletionHandler{
    return NO;
}
- (BOOL)requestMediaCapturerAccessWithCompletionHandler:(void (^)(BOOL, NSError*))handler {
    AVAuthorizationStatus videoAuthorStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    AVAuthorizationStatus audioAuthorStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];

    if (AVAuthorizationStatusAuthorized == videoAuthorStatus && AVAuthorizationStatusAuthorized == audioAuthorStatus) {
        handler(YES,nil);
    }else{
        if (AVAuthorizationStatusRestricted == videoAuthorStatus || AVAuthorizationStatusDenied == videoAuthorStatus) {
            NSString *errMsg = NSLocalizedString(@"此应用需要访问摄像头，请设置", @"此应用需要访问摄像头，请设置");
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey:errMsg};
            NSError *error = [NSError errorWithDomain:@"访问权限" code:0 userInfo:userInfo];
            handler(NO,error);

            return NO;
        }

        if (AVAuthorizationStatusRestricted == audioAuthorStatus || AVAuthorizationStatusDenied == audioAuthorStatus) {
            NSString *errMsg = NSLocalizedString(@"此应用需要访问麦克风，请设置", @"此应用需要访问麦克风，请设置");
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey:errMsg};
            NSError *error = [NSError errorWithDomain:@"访问权限" code:0 userInfo:userInfo];
            handler(NO,error);

            return NO;
        }

        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                    if (granted) {
                        handler(YES,nil);
                    }else{
                        NSString *errMsg = NSLocalizedString(@"不允许访问麦克风", @"不允许访问麦克风");
                        NSDictionary *userInfo = @{NSLocalizedDescriptionKey:errMsg};
                        NSError *error = [NSError errorWithDomain:@"访问权限" code:0 userInfo:userInfo];
                        handler(NO,error);
                    }
                }];
            }else{
                NSString *errMsg = NSLocalizedString(@"不允许访问摄像头", @"不允许访问摄像头");
                NSDictionary *userInfo = @{NSLocalizedDescriptionKey:errMsg};
                NSError *error = [NSError errorWithDomain:@"访问权限" code:0 userInfo:userInfo];
                handler(NO,error);
            }
        }];

    }
    return YES;
}

/**
 初始化
 */
- (NSError*)videoInit{
    NSString* _streamUrl = @"" ;
    _mediaCapture = [[LSMediaCapture alloc]initLiveStream:_streamUrl]; //初始化推流
    if (_mediaCapture == nil) {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"初始化失败" forKey:NSLocalizedDescriptionKey];
            NSError *error = [NSError errorWithDomain:@"LSMediaCaptureErrorDomain" code:0 userInfo:userInfo];
        return error;
    }
    return nil;
}

/**
 初始化 - 带参数
 */
- (NSError*)videoInit:(LSLiveStreamingParaCtxConfiguration *)streamparaCtx{
    NSString* _streamUrl = @"" ;
    _mediaCapture = [[LSMediaCapture alloc]initLiveStream:_streamUrl withLivestreamParaCtxConfiguration:streamparaCtx]; //初始化推流
    if (_mediaCapture == nil) {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"初始化失败" forKey:NSLocalizedDescriptionKey];
            NSError *error = [NSError errorWithDomain:@"LSMediaCaptureErrorDomain" code:0 userInfo:userInfo];
        return error;
    }
    return nil;
}

/**
 反初始化：释放资源
 */
-(void)unInitLiveStream{
    [_mediaCapture unInitLiveStream];
}
/**
 打开预览
 */
- (NSError*)startVideoPreview:(UIView*)view{
    if (_mediaCapture == nil) {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"初始化失败" forKey:NSLocalizedDescriptionKey];
            NSError *error = [NSError errorWithDomain:@"LSMediaCaptureErrorDomain" code:0 userInfo:userInfo];
        return error;
    }
    [_mediaCapture startVideoPreview:view];
    return nil;
}

/**
 重新设置url
 */
- (BOOL) setPushUrl:(NSString*)url{
    _mediaCapture.pushUrl = url;
    return YES;
}

/**
 *  开始直播
 *
 *  @param completionBlock 具体错误信息
 */
- (void)startLiveStream:(void(^)(NSError *error))completionBlock;{
    [_mediaCapture startLiveStream:completionBlock];
}

/**
 *  结束推流
 * @warning 只有直播真正开始后，也就是收到LSLiveStreamingStarted消息后，才可以关闭直播,error为nil的时候，说明直播结束，否则直播过程中发生错误
 */
- (void)stopLiveStream:(void(^)(NSError *error))completionBlock{
    [_mediaCapture stopLiveStream:completionBlock];
}

/**
开始录制并保存本地文件（mp4录制）

@param recordFileName 本地录制的文件全路径
@param videoStreamingQuality 需要录制的mp4文件的分辨率，不能大于采集的分辨率
@return 是否成功
*/
- (BOOL)startRecord:(NSString *)recordFileName videoStreamingQuality:(LSVideoStreamingQuality)videoStreamingQuality{
    return [_mediaCapture startRecord:recordFileName videoStreamingQuality:videoStreamingQuality];
}

/**
 *  停止本地录制
 */
- (BOOL)stopRecord{
    return [_mediaCapture stopRecord];
}

/**
 *  切换前后摄像头
 *
 *  @return 当前摄像头的位置，前或者后
 */
- (LSCameraPosition)switchCamera:(LSSwitchModuleVideoCameraPositionBlock)cameraPostionBlock{
    return [_mediaCapture switchCamera:cameraPostionBlock];
}

@end
