//
//  ViewController.m
//  video
//
//  Created by Timfeng on 2021/1/28.
//

#import "ViewController.h"
#import "GdpaVideoHeader.h"

#import <NMCLiveStreaming/NMCLiveStreaming.h>
#import "NEMediaCaptureEntity.h"
#import <mediaplayer/mediaplayer.h>

@interface ViewController ()

@end

@implementation ViewController{
    GdpaVideoSdk *sdk;
    NSString *savePath;
    MPMoviePlayerController *playerController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sdk = [[GdpaVideoSdk alloc]init];
    BOOL success = [sdk requestMediaCapturerAccessWithCompletionHandler:^(BOOL value, NSError* error){
        if (error) {
            NSLog(error.localizedDescription);
        }
    }];
    if(success!=YES){
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertController *aleView=[UIAlertController alertControllerWithTitle:@"提示" message:@"请授权摄像头和麦克风" preferredStyle:UIAlertControllerStyleAlert];
        [aleView addAction:cancel];
        
        [self presentViewController:aleView animated:YES completion:nil];
    }
    
    [sdk videoInit];
}

- (IBAction)click:(UIButton *)sender {
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(150, 300, 100, 100);
    [self.view addSubview:view];
    
    [sdk startVideoPreview:view];
}


- (IBAction)psh:(UIButton *)sender {
    [sdk setPushUrl:@"rtmp://push.gd-pa.cn/test/123?auth_key=1612248938-0-0-9e54a7f1aa319f57cf2b39c86331cc92"];
    [sdk startLiveStream:^(NSError* error){
        if (error) {
            NSLog(error.localizedDescription);
        }
    }];
    //录制
    LSVideoStreamingQuality qua = LS_VIDEO_QUALITY_HIGH;
    NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = arr[0];
    savePath = [documentsPath stringByAppendingString:@"/temp.mp4"];
    BOOL success = [sdk startRecord:savePath videoStreamingQuality:qua];
    if(success==NO){
        NSLog(@"录制出错");
    }

}

- (IBAction)stop:(UIButton *)sender {
    [sdk stopLiveStream:^(NSError* error){
        if (error) {
            NSLog(error.localizedDescription);
        }
    }];
    [sdk stopRecord];
    

    //播放
    NSURL *localVideoUrl = [NSURL fileURLWithPath:savePath];
    playerController =[[MPMoviePlayerController alloc]initWithContentURL:localVideoUrl];
    playerController.view.frame = CGRectMake(0, 10, 300, 300);
    [self.view addSubview: playerController.view];
    [playerController  prepareToPlay];
}
- (IBAction)swi:(UIButton *)sender {
    [sdk switchCamera:^{
        NSLog(@"切换摄像头");
    }];
}

-(void) result:(BOOL)n msg:(NSError*)m{
    
}
@end
