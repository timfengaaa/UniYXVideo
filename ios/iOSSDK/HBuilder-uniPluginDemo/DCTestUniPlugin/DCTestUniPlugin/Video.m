//
//  Video.m
//  DCTestUniPlugin
//
//  Created by Timfeng on 2021/2/1.
//  Copyright © 2021 DCloud. All rights reserved.
//

#import "Video.h"
#import "GdpaVideoHeader.h"

@implementation Video{
    UIView * view;
    GdpaVideoSdk* sdk;
}

- (UIView *)loadView {
    view = [UIView new];
    return view;
}

- (void)viewDidLoad {
    sdk = [GdpaVideoSdk new];
}

/**
 获取权限
 */
UNI_EXPORT_METHOD(@selector(requestCam:)) // 通过 WX_EXPORT_METHOD 将方法暴露给前端
-(void)requestCam:(NSDictionary *)options{
    [sdk requestMediaCapturerAccessWithCompletionHandler:^(BOOL value, NSError* error){
        if (error) {
            NSLog(error.localizedDescription);
        }
    }];
}

/**
 初始化
 */
UNI_EXPORT_METHOD(@selector(videoInit:)) // 通过 WX_EXPORT_METHOD 将方法暴露给前端
- (void)videoInit:(NSDictionary *)options{
    NSLog(@"%@",options);
    
    [sdk videoInit];
    [sdk startVideoPreview:view];
    
}

/**
 销毁初始化
 */
UNI_EXPORT_METHOD(@selector(videoUnInit:)) // 通过 WX_EXPORT_METHOD 将方法暴露给前端
- (void)videoUnInit:(NSDictionary *)options{
    NSLog(@"%@",options);
    
    [sdk unInitLiveStream];
    
}

/**
 初始化 - 参数
 */
UNI_EXPORT_METHOD(@selector(videoInitParam:callback:)) // 通过 WX_EXPORT_METHOD 将方法暴露给前端
- (void)videoInitParam:(NSDictionary *)options callback:(UniModuleKeepAliveCallback)callback{
    NSLog(@"%@",options);
    if(options.count==0){
        NSLog(@"键值为空");
    }
    LSLiveStreamingParaCtxConfiguration* ctx = [LSLiveStreamingParaCtxConfiguration new];
    ctx.sLSVideoParaCtx.cameraPosition = LS_CAMERA_POSITION_FRONT;//默认前置
    ctx.sLSVideoParaCtx.cameraPosition = LS_VIDEO_QUALITY_HIGH;//默认高清
    ctx.uploadLog = NO;//默认不上传日志
    if([[options allKeys]containsObject:@"frontCamera"]){
        
    }
    [sdk videoInit:ctx];
    [sdk startVideoPreview:view];
    
    if (callback) {
           // 第一个参数为回传给js端的数据，第二个参数为标识，表示该回调方法是否支持多次调用，如果原生端需要多次回调js端则第二个参数传 YES;
            callback(@"success",NO);
        }
}

/**
 推流
 */
UNI_EXPORT_METHOD(@selector(startStream:callback:)) // 通过 WX_EXPORT_METHOD 将方法暴露给前端
- (void)startStream:(NSDictionary *)options callback:(UniModuleKeepAliveCallback)callback{
    NSLog(@"%@",options);
    if(options.count==0){
        NSLog(@"键值为空");
    }
    [sdk setPushUrl:[options objectForKey: @"url"]];
    [sdk startLiveStream:^(NSError* error){
        if (error) {
            NSLog(error.localizedDescription);
        }
    }];
    //录制
    LSVideoStreamingQuality qua = LS_VIDEO_QUALITY_HIGH;
    NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = arr[0];
    NSString *savePath = [documentsPath stringByAppendingString:[options objectForKey: @"save"]];
    BOOL success = [sdk startRecord:savePath videoStreamingQuality:qua];
    if(success==NO){
        NSLog(@"录制出错");
    }
    
    if (callback) {
            callback(savePath,NO);//返回保存路径
    }
}
/**
 停止推流
 */
UNI_EXPORT_METHOD(@selector(stopStream:callback:)) // 通过 WX_EXPORT_METHOD 将方法暴露给前端
- (void)stopStream:(NSDictionary *)options callback:(UniModuleKeepAliveCallback)callback{
    [sdk stopLiveStream:^(NSError* error){
        if (error) {
            NSLog(error.localizedDescription);
        }
    }];
    [sdk stopRecord];
    
    if (callback) {
            callback(@"success",NO);//返回保存路径
    }
}

/**
 切换摄像头
 */
UNI_EXPORT_METHOD(@selector(switchCamera:)) // 通过 WX_EXPORT_METHOD 将方法暴露给前端
- (void)switchCamera:(NSDictionary *)options{
    [sdk switchCamera:^{
        NSLog(@"切换摄像头");
    }];
}
@end

