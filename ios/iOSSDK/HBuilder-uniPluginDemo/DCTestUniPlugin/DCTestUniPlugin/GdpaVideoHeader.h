//
//  GdpaVideoHeader.h
//  video
//
//  Created by Timfeng on 2021/1/29.
//

#ifndef GdpaVideoHeader_h
#define GdpaVideoHeader_h

#import <NMCLiveStreaming/NMCLiveStreaming.h>

@interface GdpaVideoSdk:NSObject{
    
}
@property (nonatomic,strong) LSMediaCapture* mediaCapture;

- (BOOL)requestMediaCapturerAccessWithCompletionHandler:(void (^)(BOOL, NSError*))handler ;
- (NSError*)videoInit;
- (NSError*)videoInit:(LSLiveStreamingParaCtxConfiguration *)streamparaCtx;
-(void)unInitLiveStream;
- (NSError*)startVideoPreview:(UIView*)view;
- (BOOL) setPushUrl:(NSString*)url;
- (void)startLiveStream:(void(^)(NSError *error))completionBlock;
- (void)stopLiveStream:(void(^)(NSError *error))completionBlock;
- (BOOL)startRecord:(NSString *)recordFileName videoStreamingQuality:(LSVideoStreamingQuality)videoStreamingQuality;
- (BOOL)stopRecord;

- (LSCameraPosition)switchCamera:(LSSwitchModuleVideoCameraPositionBlock)cameraPostionBlock;
@end

#endif /* GdpaVideoHeader_h */

